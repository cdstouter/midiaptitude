# run this like so: bash build.sh NoDouble (or other plugin name)

PLUGIN=$1
PLUGIN_UPPERCASE=${1^^}

VST_SDK_DIR=../../vstsdk2.4

JUCE_DIR="../../JUCE-4.2.3"
INCLUDE_DIRS="-I$JUCE_DIR -I$JUCE_DIR/modules -I$VST_SDK_DIR -I/usr/include/freetype2"
WORK_DIR=`pwd`
JUCE_VARS="-DJUCE_APP_CONFIG_HEADER=\"$WORK_DIR/Common/AppConfig.h\""
OPTIONS="-DMODULE_$PLUGIN_UPPERCASE"
LIBS="-lfreetype -lX11 -lXinerama"

mkdir -p build
cd build

# build files
echo Building...
g++ $INCLUDE_DIRS $OPTIONS $JUCE_VARS -c ../$PLUGIN/$PLUGIN.cpp || exit 1
g++ $INCLUDE_DIRS $OPTIONS $JUCE_VARS -c $JUCE_DIR/modules/juce_audio_basics/juce_audio_basics.cpp || exit 1
g++ $INCLUDE_DIRS $OPTIONS $JUCE_VARS -c $JUCE_DIR/modules/juce_audio_plugin_client/VST/juce_VST_Wrapper.cpp || exit 1
g++ $INCLUDE_DIRS $OPTIONS $JUCE_VARS -c $JUCE_DIR/modules/juce_audio_plugin_client/utility/juce_PluginUtilities.cpp || exit 1
g++ $INCLUDE_DIRS $OPTIONS $JUCE_VARS -c $JUCE_DIR/modules/juce_audio_processors/juce_audio_processors.cpp || exit 1
g++ $INCLUDE_DIRS $OPTIONS $JUCE_VARS -c $JUCE_DIR/modules/juce_core/juce_core.cpp || exit 1
g++ $INCLUDE_DIRS $OPTIONS $JUCE_VARS -c $JUCE_DIR/modules/juce_data_structures/juce_data_structures.cpp || exit 1
g++ $INCLUDE_DIRS $OPTIONS $JUCE_VARS -c $JUCE_DIR/modules/juce_events/juce_events.cpp || exit 1
g++ $INCLUDE_DIRS $OPTIONS $JUCE_VARS -c $JUCE_DIR/modules/juce_graphics/juce_graphics.cpp || exit 1
g++ $INCLUDE_DIRS $OPTIONS $JUCE_VARS -c $JUCE_DIR/modules/juce_gui_basics/juce_gui_basics.cpp || exit 1

cd ..

mkdir -p dist

g++ build/*.o $LIBS -shared -o dist/$PLUGIN.so || exit 1

rm -rf build
