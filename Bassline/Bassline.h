/* 
 * File:   Bassline.h
 * Author: Caleb
 *
 * Created on March 16, 2016, 9:37 AM
 */

#ifndef BASSLINE_H
#define BASSLINE_H

#include "../Common/JuceHeaders.h"

#ifdef DEBUG_CONSOLE
#include "../Utils/CConsole.h"
#endif

#include "../Utils/AudioParameterNote.cpp"

class Bassline  : public AudioProcessor
{
public:
    //==============================================================================
    Bassline();
    ~Bassline();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

    void processBlock (AudioSampleBuffer&, MidiBuffer&) override;

    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool silenceInProducesSilenceOut() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;
    
    // parameters
    //AudioParameterChoice* doubleMode;
    static const int numControls = 7;
    AudioParameterInt* cc[numControls];
    //AudioParameterInt* note[numControls];
    AudioParameterNote* note[numControls];
    AudioParameterInt* velocity[numControls];

private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Bassline)

    bool noteSounding[128];
    int currentCC;
    bool currentCCValue;

#ifdef DEBUG_CONSOLE
    CConsole debugConsole;
#endif
};

#endif /* BASSLINE_H */

