/* 
 * File:   Bassline.cpp
 * Author: Caleb
 *
 * Created on March 16, 2016, 9:37 AM
 */

#ifdef MODULE_BASSLINE

#include "Bassline.h"

#include <string>
#include <sstream>

//==============================================================================

Bassline::Bassline() {
    for (int i = 0; i < 128; i++) {
        noteSounding[i] = false;
    }
    currentCC = -1;
    currentCCValue = false;
    
    std::ostringstream temp;
    std::string id, name;
    for (int i=0; i<numControls; i++) {
        temp.str("");
        temp.clear();
        temp << "cc" << (i + 1);
        id = temp.str();
        temp.str("");
        temp.clear();
        temp << "CC #" << (i + 1);
        name = temp.str();
        cc[i] = new AudioParameterInt(id, name, 0, 127, 0);
        addParameter(cc[i]);
        temp.str("");
        temp.clear();
        temp << "note" << (i + 1);
        id = temp.str();
        temp.str("");
        temp.clear();
        temp << "Note #" << (i + 1);
        name = temp.str();
        //note[i] = new AudioParameterInt(id, name, 0, 127, 0);
        note[i] = new AudioParameterNote(id, name, 0, 127, 0);
        addParameter(note[i]);
        temp.str("");
        temp.clear();
        temp << "velocity" << (i + 1);
        id = temp.str();
        temp.str("");
        temp.clear();
        temp << "Velocity #" << (i + 1);
        name = temp.str();
        velocity[i] = new AudioParameterInt(id, name, 0, 127, 0);
        addParameter(velocity[i]);
    }
}

Bassline::~Bassline() { }

//==============================================================================

const String Bassline::getName() const {
    return JucePlugin_Name;
}

bool Bassline::acceptsMidi() const {
#if JucePlugin_WantsMidiInput
    return true;
#else
    return false;
#endif
}

bool Bassline::producesMidi() const {
#if JucePlugin_ProducesMidiOutput
    return true;
#else
    return false;
#endif
}

bool Bassline::silenceInProducesSilenceOut() const {
    return false;
}

double Bassline::getTailLengthSeconds() const {
    return 0.0;
}

int Bassline::getNumPrograms() {
    return 1; // NB: some hosts don't cope very well if you tell them there are 0 programs,
    // so this should be at least 1, even if you're not really implementing programs.
}

int Bassline::getCurrentProgram() {
    return 0;
}

void Bassline::setCurrentProgram(int index) { }

const String Bassline::getProgramName(int index) {
    return String();
}

void Bassline::changeProgramName(int index, const String& newName) { }

//==============================================================================

void Bassline::prepareToPlay(double sampleRate, int samplesPerBlock) {
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
}

void Bassline::releaseResources() {
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

void Bassline::processBlock(AudioSampleBuffer& buffer, MidiBuffer& midiMessages) {
    // process the midi!
    MidiBuffer::Iterator midiMessagesIterator(midiMessages);
    MidiMessage midiMessage(0);
    MidiBuffer midiOut;
    int sampleNumber;
    while (midiMessagesIterator.getNextEvent(midiMessage, sampleNumber)) {
        if (midiMessage.isController()) {
            if (midiMessage.getControllerNumber() == 123) {
                // all notes off
                // reset our internal state
                for (int i = 0; i < 128; i++) {
                    noteSounding[i] = false;
                }
                // and pass it through
                midiOut.addEvent(midiMessage, sampleNumber);
            }
            // have any of our CCs happened?
            bool respondToCC = false;
            for (int i=0; i<numControls; i++) {
                if (midiMessage.getControllerNumber() == cc[i]->get()) {
                    respondToCC = true;
                    break;
                }
            }
            // if it's a new one, silence the old notes and start the new ones
            if (respondToCC) {
                bool newValue = midiMessage.getControllerValue() >= 64;
                if ((midiMessage.getControllerNumber() != currentCC) || (newValue && !currentCCValue)) {
                    int oldSampleNumber = sampleNumber - 1;
                    if (oldSampleNumber < 0) oldSampleNumber = 0;
                    int newSampleNumber = oldSampleNumber + 1;
                    // turn off any currently playing notes
                    for (int i = 0; i < 128; i++) {
                        if (noteSounding[i]) {
                            midiOut.addEvent(midiMessage.noteOff(1, i), oldSampleNumber);
                            noteSounding[i] = false;
                        }
                    }
                    // set the current CC
                    currentCC = midiMessage.getControllerNumber();
                    // start up the new notes
                    for (int i=0; i<numControls; i++) {
                        if (cc[i]->get() == currentCC) {
                            // only turn on the note if velocity > 0
                            if (velocity[i]->get() > 0) {
                                // turn on the note
                                midiOut.addEvent(midiMessage.noteOn(1, note[i]->get(), (uint8)velocity[i]->get()), newSampleNumber);
                                noteSounding[note[i]->get()] = true;
                            }
                        }
                    }
                }
                currentCCValue = newValue;
            }
        }
    }
    midiMessages.clear();
    midiMessages = midiOut;
}

//==============================================================================

bool Bassline::hasEditor() const {
    return false;
}

AudioProcessorEditor* Bassline::createEditor() {
    return 0;
}

//==============================================================================

void Bassline::getStateInformation(MemoryBlock& destData) {
    // Create an outer XML element..
    XmlElement xml("MIDIAPTITUDE_BASSLINE_SETTINGS");

    // Store the values of all our parameters, using their param ID as the XML attribute
    for (int i = 0; i < getNumParameters(); ++i)
        if (AudioProcessorParameterWithID * p = dynamic_cast<AudioProcessorParameterWithID*>(getParameters().getUnchecked(i)))
            xml.setAttribute(p->paramID, p->getValue());

    // then use this helper function to stuff it into the binary blob and return it..
    copyXmlToBinary(xml, destData);
}

void Bassline::setStateInformation(const void* data, int sizeInBytes) {
    // This getXmlFromBinary() helper function retrieves our XML from the binary blob..
    ScopedPointer<XmlElement> xmlState(getXmlFromBinary(data, sizeInBytes));

    if (xmlState != nullptr) {
        // make sure that it's actually our type of XML object..
        if (xmlState->hasTagName("MIDIAPTITUDE_BASSLINE_SETTINGS")) {
            // Now reload our parameters..
            for (int i = 0; i < getNumParameters(); ++i)
                if (AudioProcessorParameterWithID * p = dynamic_cast<AudioProcessorParameterWithID*>(getParameters().getUnchecked(i)))
                    p->setValueNotifyingHost((float)xmlState->getDoubleAttribute(p->paramID, p->getValue()));
        }
    }
}

//==============================================================================
// This creates new instances of the plugin..

AudioProcessor* JUCE_CALLTYPE createPluginFilter() {
    return new Bassline();
}

#endif