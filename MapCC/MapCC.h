/* 
 * File:   MapCC.h
 * Author: Caleb
 *
 * Created on September 17, 2016, 4:39 PM
 */

#ifndef MAPCC_H
#define MAPCC_H

#include "../Common/JuceHeaders.h"

#ifdef DEBUG_CONSOLE
#include "../Utils/CConsole.h"
#endif

class MapCC  : public AudioProcessor
{
public:
    //==============================================================================
    MapCC();
    ~MapCC();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

    void processBlock (AudioSampleBuffer&, MidiBuffer&) override;

    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool silenceInProducesSilenceOut() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;
    
    // parameters
    AudioParameterInt* paramInputCC;
    AudioParameterInt* paramInputLow;
    AudioParameterInt* paramInputHigh;
    AudioParameterBool* paramInputClamp;
    AudioParameterInt* paramOutputCC;
    AudioParameterInt* paramOutputLow;
    AudioParameterInt* paramOutputHigh;
    AudioParameterBool* paramPassNotes;
    AudioParameterBool* paramPassOtherCCs;
    AudioParameterBool* paramPassElse;

private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MapCC)

#ifdef DEBUG_CONSOLE
    CConsole debugConsole;
#endif
};

#endif /* MAPCC_H */

