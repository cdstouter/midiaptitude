/* 
 * File:   MapCC.cpp
 * Author: Caleb
 * 
 * Created on September 17, 2016, 4:39 PM
 */

#ifdef MODULE_MAPCC

#include "MapCC.h"

//==============================================================================

MapCC::MapCC() {
    paramInputCC = new AudioParameterInt("inputcc", "Input CC", 1, 127, 10);
    addParameter(paramInputCC);
    paramInputLow = new AudioParameterInt("inputlow", "Input Low Value", 0, 127, 0);
    addParameter(paramInputLow);
    paramInputHigh = new AudioParameterInt("inputhigh", "Input High Value", 0, 127, 127);
    addParameter(paramInputHigh);
    paramInputClamp = new AudioParameterBool("inputclamp", "Clamp Input Values", false);
    addParameter(paramInputClamp);
    paramOutputCC = new AudioParameterInt("outputcc", "Output CC, 0 is no change", 0, 127, 0);
    addParameter(paramOutputCC);
    paramOutputLow = new AudioParameterInt("outputlow", "Output Low Value", 0, 127, 0);
    addParameter(paramOutputLow);
    paramOutputHigh = new AudioParameterInt("outputhigh", "Output High Value", 0, 127, 127);
    addParameter(paramOutputHigh);
    paramPassNotes = new AudioParameterBool("passnotes", "Pass Notes", true);
    addParameter(paramPassNotes);
    paramPassOtherCCs = new AudioParameterBool("passotherccs", "Pass Other CCs", true);
    addParameter(paramPassOtherCCs);
    paramPassElse = new AudioParameterBool("passelse", "Pass Everything Else", true);
    addParameter(paramPassElse);
}

MapCC::~MapCC() { }

//==============================================================================

const String MapCC::getName() const {
    return JucePlugin_Name;
}

bool MapCC::acceptsMidi() const {
#if JucePlugin_WantsMidiInput
    return true;
#else
    return false;
#endif
}

bool MapCC::producesMidi() const {
#if JucePlugin_ProducesMidiOutput
    return true;
#else
    return false;
#endif
}

bool MapCC::silenceInProducesSilenceOut() const {
    return false;
}

double MapCC::getTailLengthSeconds() const {
    return 0.0;
}

int MapCC::getNumPrograms() {
    return 1; // NB: some hosts don't cope very well if you tell them there are 0 programs,
    // so this should be at least 1, even if you're not really implementing programs.
}

int MapCC::getCurrentProgram() {
    return 0;
}

void MapCC::setCurrentProgram(int index) { }

const String MapCC::getProgramName(int index) {
    return String();
}

void MapCC::changeProgramName(int index, const String& newName) { }

//==============================================================================

void MapCC::prepareToPlay(double sampleRate, int samplesPerBlock) {
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
}

void MapCC::releaseResources() {
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

void MapCC::processBlock(AudioSampleBuffer& buffer, MidiBuffer& midiMessages) {
    // process the midi!
    MidiBuffer::Iterator midiMessagesIterator(midiMessages);
    MidiMessage midiMessage(0);
    MidiBuffer midiOut;
    int sampleNumber;
    int in_cc = paramInputCC->get();
    int in_low = paramInputLow->get(), in_high = paramInputHigh->get();
    bool in_clamp = paramInputClamp->get();
    while (midiMessagesIterator.getNextEvent(midiMessage, sampleNumber)) {
		if (midiMessage.isController() && midiMessage.getControllerNumber() == in_cc) {
			// if it's within the input range, modify it
			int cv = midiMessage.getControllerValue();
			if (in_clamp || (cv >= in_low && cv <= in_high)) {
        if (cv < in_low) cv = in_low;
        if (cv > in_high) cv = in_high;
				int out_low = paramOutputLow->get(), out_high = paramOutputHigh->get();
				int out_cc = paramOutputCC->get();
				float pos = (float)(cv - in_low) / (float)(in_high - in_low);
				int out_cv = (int)(pos * (float)(out_high - out_low)) + out_low;
				if (out_cc == 0) out_cc = in_cc;
				midiOut.addEvent(midiMessage.controllerEvent(midiMessage.getChannel(), out_cc, out_cv), sampleNumber);
			} else {
				// pass it through unmodified
				midiOut.addEvent(midiMessage, sampleNumber);
			}
        } else if (midiMessage.isController()) {
            // it's another CC, only pass it through if desired
            if (paramPassOtherCCs->get()) midiOut.addEvent(midiMessage, sampleNumber);
        } else if (midiMessage.isNoteOnOrOff()) {
            // it's a note, only pass it through if desired
            if (paramPassNotes->get()) midiOut.addEvent(midiMessage, sampleNumber);
		} else {
			// it's something else, only pass it through if desired
            if (paramPassElse->get()) midiOut.addEvent(midiMessage, sampleNumber);
		}
    }
    midiMessages.clear();
    midiMessages = midiOut;
}

//==============================================================================

bool MapCC::hasEditor() const {
    return false;
}

AudioProcessorEditor* MapCC::createEditor() {
    return 0;
}

//==============================================================================

void MapCC::getStateInformation(MemoryBlock& destData) {
    // Create an outer XML element..
    XmlElement xml("MIDIAPTITUDE_MAPCC_SETTINGS");

    // Store the values of all our parameters, using their param ID as the XML attribute
    for (int i = 0; i < getNumParameters(); ++i)
        if (AudioProcessorParameterWithID * p = dynamic_cast<AudioProcessorParameterWithID*>(getParameters().getUnchecked(i)))
            xml.setAttribute(p->paramID, p->getValue());

    // then use this helper function to stuff it into the binary blob and return it..
    copyXmlToBinary(xml, destData);
}

void MapCC::setStateInformation(const void* data, int sizeInBytes) {
    // This getXmlFromBinary() helper function retrieves our XML from the binary blob..
    ScopedPointer<XmlElement> xmlState(getXmlFromBinary(data, sizeInBytes));

    if (xmlState != nullptr) {
        // make sure that it's actually our type of XML object..
        if (xmlState->hasTagName("MIDIAPTITUDE_MAPCC_SETTINGS")) {
            // Now reload our parameters..
            for (int i = 0; i < getNumParameters(); ++i)
                if (AudioProcessorParameterWithID * p = dynamic_cast<AudioProcessorParameterWithID*>(getParameters().getUnchecked(i)))
                    p->setValueNotifyingHost((float)xmlState->getDoubleAttribute(p->paramID, p->getValue()));
        }
    }
}

//==============================================================================
// This creates new instances of the plugin..

AudioProcessor* JUCE_CALLTYPE createPluginFilter() {
    return new MapCC();
}

#endif
