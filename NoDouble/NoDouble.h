/* 
 * File:   NoDouble.h
 * Author: Caleb
 *
 * Created on March 10, 2016, 8:43 AM
 */

#ifndef NODOUBLE_H
#define NODOUBLE_H

#include "../Common/JuceHeaders.h"

#ifdef DEBUG_CONSOLE
#include "../Utils/CConsole.h"
#endif

class NoDouble  : public AudioProcessor
{
public:
    //==============================================================================
    NoDouble();
    ~NoDouble();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

    void processBlock (AudioSampleBuffer&, MidiBuffer&) override;

    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool silenceInProducesSilenceOut() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;
    
    // parameters
    AudioParameterChoice* doubleMode;

private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (NoDouble)

    bool notePressed[128];
    bool noteSounding[128];
    bool sustainPressed;

#ifdef DEBUG_CONSOLE
    CConsole debugConsole;
#endif
};

#endif /* NODOUBLE_H */

