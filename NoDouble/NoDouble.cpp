/* 
 * File:   NoDouble.cpp
 * Author: Caleb
 * 
 * Created on March 10, 2016, 8:43 AM
 */

#ifdef MODULE_NODOUBLE

#include "NoDouble.h"

//==============================================================================

NoDouble::NoDouble() {
    for (int i = 0; i < 128; i++) {
        notePressed[i] = noteSounding[i] = false;
    }
    sustainPressed = false;
    StringArray choices;
    choices.add("Ignore");
    choices.add("Replace");
    doubleMode = new AudioParameterChoice("mode", "Mode", choices, 0);
    addParameter(doubleMode);
}

NoDouble::~NoDouble() { }

//==============================================================================

const String NoDouble::getName() const {
    return JucePlugin_Name;
}

bool NoDouble::acceptsMidi() const {
#if JucePlugin_WantsMidiInput
    return true;
#else
    return false;
#endif
}

bool NoDouble::producesMidi() const {
#if JucePlugin_ProducesMidiOutput
    return true;
#else
    return false;
#endif
}

bool NoDouble::silenceInProducesSilenceOut() const {
    return false;
}

double NoDouble::getTailLengthSeconds() const {
    return 0.0;
}

int NoDouble::getNumPrograms() {
    return 1; // NB: some hosts don't cope very well if you tell them there are 0 programs,
    // so this should be at least 1, even if you're not really implementing programs.
}

int NoDouble::getCurrentProgram() {
    return 0;
}

void NoDouble::setCurrentProgram(int index) { }

const String NoDouble::getProgramName(int index) {
    return String();
}

void NoDouble::changeProgramName(int index, const String& newName) { }

//==============================================================================

void NoDouble::prepareToPlay(double sampleRate, int samplesPerBlock) {
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
}

void NoDouble::releaseResources() {
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

void NoDouble::processBlock(AudioSampleBuffer& buffer, MidiBuffer& midiMessages) {
    // process the midi!
    MidiBuffer::Iterator midiMessagesIterator(midiMessages);
    MidiMessage midiMessage(0);
    MidiBuffer midiOut;
    int sampleNumber;
    while (midiMessagesIterator.getNextEvent(midiMessage, sampleNumber)) {
        // first of all, everything goes to channel 1
        if (midiMessage.getChannel() > 1) midiMessage.setChannel(1);
        if (midiMessage.isNoteOn()) {
            int note = midiMessage.getNoteNumber();
            // note on
            notePressed[note] = true;
            if (!noteSounding[note]) {
                // sound the note
                midiOut.addEvent(midiMessage, sampleNumber);
                noteSounding[note] = true;
            } else if (doubleMode->getIndex() == 1) {
                // we're in replace mode, so replace the note with a new one
                // the note off message happens one sample earlier than note on
                int offSampleNumber = sampleNumber - 1;
                if (offSampleNumber < 0) offSampleNumber = 0;
                midiOut.addEvent(midiMessage.noteOff(1, note), offSampleNumber);
                midiOut.addEvent(midiMessage, offSampleNumber + 1);
            }
        } else if (midiMessage.isNoteOff()) {
            int note = midiMessage.getNoteNumber();
            // note off
            notePressed[note] = 0;
            if (noteSounding[note] && !sustainPressed) {
                midiOut.addEvent(midiMessage, sampleNumber);
                noteSounding[note] = false;
            }
        } else if (midiMessage.isController() && midiMessage.getControllerNumber() == 64) {
            // sustain pedal CC
            if (midiMessage.getControllerValue() < 64) {
                // sustain pedal is up
                sustainPressed = false;
                // do we have any notes to release?
                for (int i = 0; i < 128; i++) {
                    if (noteSounding[i] && !notePressed[i]) {
                        midiOut.addEvent(midiMessage.noteOff(1, i), sampleNumber);
                        noteSounding[i] = false;
                    }
                }
            } else {
                // sustain pedal is down
                sustainPressed = true;
            }
        } else if (midiMessage.isController() && midiMessage.getControllerNumber() == 123) {
            // all notes off CC
            // reset our internal state to all notes off
            for (int i = 0; i < 128; i++) {
                noteSounding[i] = notePressed[i] = false;
            }
            // and pass it through
            midiOut.addEvent(midiMessage, sampleNumber);
        } else {
            // pass it through
            midiOut.addEvent(midiMessage, sampleNumber);
        }
    }
    midiMessages.clear();
    midiMessages = midiOut;
}

//==============================================================================

bool NoDouble::hasEditor() const {
    return false;
}

AudioProcessorEditor* NoDouble::createEditor() {
    return 0;
}

//==============================================================================

void NoDouble::getStateInformation(MemoryBlock& destData) {
    // Create an outer XML element..
    XmlElement xml("MIDIAPTITUDE_NODOUBLE_SETTINGS");

    // Store the values of all our parameters, using their param ID as the XML attribute
    for (int i = 0; i < getNumParameters(); ++i)
        if (AudioProcessorParameterWithID * p = dynamic_cast<AudioProcessorParameterWithID*>(getParameters().getUnchecked(i)))
            xml.setAttribute(p->paramID, p->getValue());

    // then use this helper function to stuff it into the binary blob and return it..
    copyXmlToBinary(xml, destData);
}

void NoDouble::setStateInformation(const void* data, int sizeInBytes) {
    // This getXmlFromBinary() helper function retrieves our XML from the binary blob..
    ScopedPointer<XmlElement> xmlState(getXmlFromBinary(data, sizeInBytes));

    if (xmlState != nullptr) {
        // make sure that it's actually our type of XML object..
        if (xmlState->hasTagName("MIDIAPTITUDE_NODOUBLE_SETTINGS")) {
            // Now reload our parameters..
            for (int i = 0; i < getNumParameters(); ++i)
                if (AudioProcessorParameterWithID * p = dynamic_cast<AudioProcessorParameterWithID*>(getParameters().getUnchecked(i)))
                    p->setValueNotifyingHost((float)xmlState->getDoubleAttribute(p->paramID, p->getValue()));
        }
    }
}

//==============================================================================
// This creates new instances of the plugin..

AudioProcessor* JUCE_CALLTYPE createPluginFilter() {
    return new NoDouble();
}

#endif