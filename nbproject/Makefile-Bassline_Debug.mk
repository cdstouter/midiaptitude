#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=MinGW32-Windows
CND_DLIB_EXT=dll
CND_CONF=Bassline_Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/97ccf7a8/juce_audio_basics.o \
	${OBJECTDIR}/_ext/d29c1b9a/juce_audio_devices.o \
	${OBJECTDIR}/_ext/4d427619/juce_audio_formats.o \
	${OBJECTDIR}/_ext/417e1ffc/juce_VST_Wrapper.o \
	${OBJECTDIR}/_ext/b6c179b1/juce_PluginUtilities.o \
	${OBJECTDIR}/_ext/cefe0244/juce_audio_processors.o \
	${OBJECTDIR}/_ext/de422ab9/juce_core.o \
	${OBJECTDIR}/_ext/c2e0c9bb/juce_data_structures.o \
	${OBJECTDIR}/_ext/5a28fdb3/juce_events.o \
	${OBJECTDIR}/_ext/694d08a5/juce_graphics.o \
	${OBJECTDIR}/_ext/b8139523/juce_gui_basics.o \
	${OBJECTDIR}/Bassline/Bassline.o \
	${OBJECTDIR}/BlockChords/BlockChords.o \
	${OBJECTDIR}/NoDouble/NoDouble.o \
	${OBJECTDIR}/Utils/CConsole.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-m32 -Wno-strict-aliasing -Wno-strict-overflow
CXXFLAGS=-m32 -Wno-strict-aliasing -Wno-strict-overflow

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lcomdlg32 -limm32 -lole32 -loleaut32 -lrpcrt4 -lshlwapi -luuid -lversion -lwininet -lwinmm -lws2_32 -lwsock32 -lgdi32

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/MidiAptitude_${CND_CONF}.${CND_DLIB_EXT}

${CND_DISTDIR}/MidiAptitude_${CND_CONF}.${CND_DLIB_EXT}: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}
	${LINK.cc} -o ${CND_DISTDIR}/MidiAptitude_${CND_CONF}.${CND_DLIB_EXT} ${OBJECTFILES} ${LDLIBSOPTIONS} -static -static-libgcc -static-libstdc++ -shared

${OBJECTDIR}/_ext/97ccf7a8/juce_audio_basics.o: ../../juce-grapefruit-windows/modules/juce_audio_basics/juce_audio_basics.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/97ccf7a8
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -DDEBUG=1 -DDEBUG_CONSOLE -DMODULE_BASSLINE -D_DEBUG=1 -I../../juce-grapefruit-windows -I../../vstsdk2.4 -ICommon -std=c++11  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/97ccf7a8/juce_audio_basics.o ../../juce-grapefruit-windows/modules/juce_audio_basics/juce_audio_basics.cpp

${OBJECTDIR}/_ext/d29c1b9a/juce_audio_devices.o: ../../juce-grapefruit-windows/modules/juce_audio_devices/juce_audio_devices.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/d29c1b9a
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -DDEBUG=1 -DDEBUG_CONSOLE -DMODULE_BASSLINE -D_DEBUG=1 -I../../juce-grapefruit-windows -I../../vstsdk2.4 -ICommon -std=c++11  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/d29c1b9a/juce_audio_devices.o ../../juce-grapefruit-windows/modules/juce_audio_devices/juce_audio_devices.cpp

${OBJECTDIR}/_ext/4d427619/juce_audio_formats.o: ../../juce-grapefruit-windows/modules/juce_audio_formats/juce_audio_formats.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/4d427619
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -DDEBUG=1 -DDEBUG_CONSOLE -DMODULE_BASSLINE -D_DEBUG=1 -I../../juce-grapefruit-windows -I../../vstsdk2.4 -ICommon -std=c++11  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4d427619/juce_audio_formats.o ../../juce-grapefruit-windows/modules/juce_audio_formats/juce_audio_formats.cpp

${OBJECTDIR}/_ext/417e1ffc/juce_VST_Wrapper.o: ../../juce-grapefruit-windows/modules/juce_audio_plugin_client/VST/juce_VST_Wrapper.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/417e1ffc
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -DDEBUG=1 -DDEBUG_CONSOLE -DMODULE_BASSLINE -D_DEBUG=1 -I../../juce-grapefruit-windows -I../../vstsdk2.4 -ICommon -std=c++11  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/417e1ffc/juce_VST_Wrapper.o ../../juce-grapefruit-windows/modules/juce_audio_plugin_client/VST/juce_VST_Wrapper.cpp

${OBJECTDIR}/_ext/b6c179b1/juce_PluginUtilities.o: ../../juce-grapefruit-windows/modules/juce_audio_plugin_client/utility/juce_PluginUtilities.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/b6c179b1
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -DDEBUG=1 -DDEBUG_CONSOLE -DMODULE_BASSLINE -D_DEBUG=1 -I../../juce-grapefruit-windows -I../../vstsdk2.4 -ICommon -std=c++11  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/b6c179b1/juce_PluginUtilities.o ../../juce-grapefruit-windows/modules/juce_audio_plugin_client/utility/juce_PluginUtilities.cpp

${OBJECTDIR}/_ext/cefe0244/juce_audio_processors.o: ../../juce-grapefruit-windows/modules/juce_audio_processors/juce_audio_processors.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/cefe0244
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -DDEBUG=1 -DDEBUG_CONSOLE -DMODULE_BASSLINE -D_DEBUG=1 -I../../juce-grapefruit-windows -I../../vstsdk2.4 -ICommon -std=c++11  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/cefe0244/juce_audio_processors.o ../../juce-grapefruit-windows/modules/juce_audio_processors/juce_audio_processors.cpp

${OBJECTDIR}/_ext/de422ab9/juce_core.o: ../../juce-grapefruit-windows/modules/juce_core/juce_core.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/de422ab9
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -DDEBUG=1 -DDEBUG_CONSOLE -DMODULE_BASSLINE -D_DEBUG=1 -I../../juce-grapefruit-windows -I../../vstsdk2.4 -ICommon -std=c++11  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/de422ab9/juce_core.o ../../juce-grapefruit-windows/modules/juce_core/juce_core.cpp

${OBJECTDIR}/_ext/c2e0c9bb/juce_data_structures.o: ../../juce-grapefruit-windows/modules/juce_data_structures/juce_data_structures.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/c2e0c9bb
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -DDEBUG=1 -DDEBUG_CONSOLE -DMODULE_BASSLINE -D_DEBUG=1 -I../../juce-grapefruit-windows -I../../vstsdk2.4 -ICommon -std=c++11  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/c2e0c9bb/juce_data_structures.o ../../juce-grapefruit-windows/modules/juce_data_structures/juce_data_structures.cpp

${OBJECTDIR}/_ext/5a28fdb3/juce_events.o: ../../juce-grapefruit-windows/modules/juce_events/juce_events.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/5a28fdb3
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -DDEBUG=1 -DDEBUG_CONSOLE -DMODULE_BASSLINE -D_DEBUG=1 -I../../juce-grapefruit-windows -I../../vstsdk2.4 -ICommon -std=c++11  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/5a28fdb3/juce_events.o ../../juce-grapefruit-windows/modules/juce_events/juce_events.cpp

${OBJECTDIR}/_ext/694d08a5/juce_graphics.o: ../../juce-grapefruit-windows/modules/juce_graphics/juce_graphics.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/694d08a5
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -DDEBUG=1 -DDEBUG_CONSOLE -DMODULE_BASSLINE -D_DEBUG=1 -I../../juce-grapefruit-windows -I../../vstsdk2.4 -ICommon -std=c++11  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/694d08a5/juce_graphics.o ../../juce-grapefruit-windows/modules/juce_graphics/juce_graphics.cpp

${OBJECTDIR}/_ext/b8139523/juce_gui_basics.o: ../../juce-grapefruit-windows/modules/juce_gui_basics/juce_gui_basics.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/b8139523
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -DDEBUG=1 -DDEBUG_CONSOLE -DMODULE_BASSLINE -D_DEBUG=1 -I../../juce-grapefruit-windows -I../../vstsdk2.4 -ICommon -std=c++11  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/b8139523/juce_gui_basics.o ../../juce-grapefruit-windows/modules/juce_gui_basics/juce_gui_basics.cpp

${OBJECTDIR}/Bassline/Bassline.o: Bassline/Bassline.cpp 
	${MKDIR} -p ${OBJECTDIR}/Bassline
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -DDEBUG=1 -DDEBUG_CONSOLE -DMODULE_BASSLINE -D_DEBUG=1 -I../../juce-grapefruit-windows -I../../vstsdk2.4 -ICommon -std=c++11  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Bassline/Bassline.o Bassline/Bassline.cpp

${OBJECTDIR}/BlockChords/BlockChords.o: BlockChords/BlockChords.cpp 
	${MKDIR} -p ${OBJECTDIR}/BlockChords
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -DDEBUG=1 -DDEBUG_CONSOLE -DMODULE_BASSLINE -D_DEBUG=1 -I../../juce-grapefruit-windows -I../../vstsdk2.4 -ICommon -std=c++11  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/BlockChords/BlockChords.o BlockChords/BlockChords.cpp

${OBJECTDIR}/NoDouble/NoDouble.o: NoDouble/NoDouble.cpp 
	${MKDIR} -p ${OBJECTDIR}/NoDouble
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -DDEBUG=1 -DDEBUG_CONSOLE -DMODULE_BASSLINE -D_DEBUG=1 -I../../juce-grapefruit-windows -I../../vstsdk2.4 -ICommon -std=c++11  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NoDouble/NoDouble.o NoDouble/NoDouble.cpp

${OBJECTDIR}/Utils/CConsole.o: Utils/CConsole.cpp 
	${MKDIR} -p ${OBJECTDIR}/Utils
	${RM} "$@.d"
	$(COMPILE.cc) -g -s -DDEBUG=1 -DDEBUG_CONSOLE -DMODULE_BASSLINE -D_DEBUG=1 -I../../juce-grapefruit-windows -I../../vstsdk2.4 -ICommon -std=c++11  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Utils/CConsole.o Utils/CConsole.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/MidiAptitude_${CND_CONF}.${CND_DLIB_EXT}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
