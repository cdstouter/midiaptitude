#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# NoDouble_Debug configuration
CND_PLATFORM_NoDouble_Debug=MinGW32-Windows
CND_ARTIFACT_DIR_NoDouble_Debug=dist
CND_ARTIFACT_NAME_NoDouble_Debug=MidiAptitude_NoDouble_Debug.dll
CND_ARTIFACT_PATH_NoDouble_Debug=dist/MidiAptitude_NoDouble_Debug.dll
CND_PACKAGE_DIR_NoDouble_Debug=dist/NoDouble_Debug/MinGW32-Windows/package
CND_PACKAGE_NAME_NoDouble_Debug=libMidiAptitude.dll.tar
CND_PACKAGE_PATH_NoDouble_Debug=dist/NoDouble_Debug/MinGW32-Windows/package/libMidiAptitude.dll.tar
# NoDouble configuration
CND_PLATFORM_NoDouble=MinGW32-Windows
CND_ARTIFACT_DIR_NoDouble=dist
CND_ARTIFACT_NAME_NoDouble=MidiAptitude_NoDouble.dll
CND_ARTIFACT_PATH_NoDouble=dist/MidiAptitude_NoDouble.dll
CND_PACKAGE_DIR_NoDouble=dist/NoDouble/MinGW32-Windows/package
CND_PACKAGE_NAME_NoDouble=libMidiAptitude.dll.tar
CND_PACKAGE_PATH_NoDouble=dist/NoDouble/MinGW32-Windows/package/libMidiAptitude.dll.tar
# Bassline_Debug configuration
CND_PLATFORM_Bassline_Debug=MinGW32-Windows
CND_ARTIFACT_DIR_Bassline_Debug=dist
CND_ARTIFACT_NAME_Bassline_Debug=MidiAptitude_Bassline_Debug.dll
CND_ARTIFACT_PATH_Bassline_Debug=dist/MidiAptitude_Bassline_Debug.dll
CND_PACKAGE_DIR_Bassline_Debug=dist/Bassline_Debug/MinGW32-Windows/package
CND_PACKAGE_NAME_Bassline_Debug=libMidiAptitude.dll.tar
CND_PACKAGE_PATH_Bassline_Debug=dist/Bassline_Debug/MinGW32-Windows/package/libMidiAptitude.dll.tar
# Bassline configuration
CND_PLATFORM_Bassline=MinGW32-Windows
CND_ARTIFACT_DIR_Bassline=dist
CND_ARTIFACT_NAME_Bassline=MidiAptitude_Bassline.dll
CND_ARTIFACT_PATH_Bassline=dist/MidiAptitude_Bassline.dll
CND_PACKAGE_DIR_Bassline=dist/Bassline/MinGW32-Windows/package
CND_PACKAGE_NAME_Bassline=libMidiAptitude.dll.tar
CND_PACKAGE_PATH_Bassline=dist/Bassline/MinGW32-Windows/package/libMidiAptitude.dll.tar
# BlockChords_Debug configuration
CND_PLATFORM_BlockChords_Debug=MinGW32-Windows
CND_ARTIFACT_DIR_BlockChords_Debug=dist
CND_ARTIFACT_NAME_BlockChords_Debug=MidiAptitude_BlockChords_Debug.dll
CND_ARTIFACT_PATH_BlockChords_Debug=dist/MidiAptitude_BlockChords_Debug.dll
CND_PACKAGE_DIR_BlockChords_Debug=dist/BlockChords_Debug/MinGW32-Windows/package
CND_PACKAGE_NAME_BlockChords_Debug=libMidiAptitude.dll.tar
CND_PACKAGE_PATH_BlockChords_Debug=dist/BlockChords_Debug/MinGW32-Windows/package/libMidiAptitude.dll.tar
# BlockChords configuration
CND_PLATFORM_BlockChords=MinGW32-Windows
CND_ARTIFACT_DIR_BlockChords=dist
CND_ARTIFACT_NAME_BlockChords=MidiAptitude_BlockChords.dll
CND_ARTIFACT_PATH_BlockChords=dist/MidiAptitude_BlockChords.dll
CND_PACKAGE_DIR_BlockChords=dist/BlockChords/MinGW32-Windows/package
CND_PACKAGE_NAME_BlockChords=libMidiAptitude.dll.tar
CND_PACKAGE_PATH_BlockChords=dist/BlockChords/MinGW32-Windows/package/libMidiAptitude.dll.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
