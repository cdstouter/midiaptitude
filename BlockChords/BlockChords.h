/* 
 * File:   BlockChords.h
 * Author: Caleb
 *
 * Created on March 16, 2016, 12:45 PM
 */

#ifndef BLOCKCHORDS_H
#define BLOCKCHORDS_H

#include "../Common/JuceHeaders.h"

#ifdef DEBUG_CONSOLE
#include "../Utils/CConsole.h"
#endif

#include "../Utils/AudioParameterNote.cpp"

class BlockChords  : public AudioProcessor
{
public:
    //==============================================================================
    BlockChords();
    ~BlockChords();

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

    void processBlock (AudioSampleBuffer&, MidiBuffer&) override;

    //==============================================================================
    AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool silenceInProducesSilenceOut() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const String getProgramName (int index) override;
    void changeProgramName (int index, const String& newName) override;

    //==============================================================================
    void getStateInformation (MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;
    
    // parameters
    AudioParameterChoice* paramKey;
    AudioParameterNote* paramLowNote;
    AudioParameterNote* paramHighNote;

private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (BlockChords)

    bool noteSounding[128];
    int currentNote;

#ifdef DEBUG_CONSOLE
    CConsole debugConsole;
#endif
};

#endif /* BLOCKCHORDS_H */

