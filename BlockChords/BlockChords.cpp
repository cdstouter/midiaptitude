/* 
 * File:   BlockChords.cpp
 * Author: Caleb
 * 
 * Created on March 16, 2016, 12:45 PM
 */

#ifdef MODULE_BLOCKCHORDS

#include "BlockChords.h"

#include <string>
#include <sstream>

//==============================================================================

BlockChords::BlockChords() {
    for (int i = 0; i < 128; i++) {
        noteSounding[i] = false;
    }
    currentNote = -1;
    StringArray choices;
    choices.add("C major/A minor");
    choices.add("Db major/Bb minor");
    choices.add("D major/B minor");
    choices.add("Eb major/C minor");
    choices.add("E major/C# minor");
    choices.add("F major/D minor");
    choices.add("Gb major/Eb minor");
    choices.add("G major/E minor");
    choices.add("Ab major/F minor");
    choices.add("A major/F# minor");
    choices.add("Bb major/G minor");
    choices.add("B major/G# minor");
    paramKey = new AudioParameterChoice("key", "Key", choices, 0);
    addParameter(paramKey);
    paramLowNote = new AudioParameterNote("lownote", "Low note", 0, 127, 60);
    addParameter(paramLowNote);
    paramHighNote = new AudioParameterNote("highnote", "Low note", 0, 127, 72);
    addParameter(paramHighNote);
}

BlockChords::~BlockChords() { }

//==============================================================================

const String BlockChords::getName() const {
    return JucePlugin_Name;
}

bool BlockChords::acceptsMidi() const {
#if JucePlugin_WantsMidiInput
    return true;
#else
    return false;
#endif
}

bool BlockChords::producesMidi() const {
#if JucePlugin_ProducesMidiOutput
    return true;
#else
    return false;
#endif
}

bool BlockChords::silenceInProducesSilenceOut() const {
    return false;
}

double BlockChords::getTailLengthSeconds() const {
    return 0.0;
}

int BlockChords::getNumPrograms() {
    return 1; // NB: some hosts don't cope very well if you tell them there are 0 programs,
    // so this should be at least 1, even if you're not really implementing programs.
}

int BlockChords::getCurrentProgram() {
    return 0;
}

void BlockChords::setCurrentProgram(int index) { }

const String BlockChords::getProgramName(int index) {
    return String();
}

void BlockChords::changeProgramName(int index, const String& newName) { }

//==============================================================================

void BlockChords::prepareToPlay(double sampleRate, int samplesPerBlock) {
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
}

void BlockChords::releaseResources() {
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

void BlockChords::processBlock(AudioSampleBuffer& buffer, MidiBuffer& midiMessages) {
    // process the midi!
    MidiBuffer::Iterator midiMessagesIterator(midiMessages);
    MidiMessage midiMessage(0);
    MidiBuffer midiOut;
    int sampleNumber;
    while (midiMessagesIterator.getNextEvent(midiMessage, sampleNumber)) {
        if (midiMessage.isController()) {
            if (midiMessage.getControllerNumber() == 123) {
                // all notes off
                // reset our internal state
                for (int i = 0; i < 128; i++) {
                    noteSounding[i] = false;
                }
                // and pass it through
                midiOut.addEvent(midiMessage, sampleNumber);
            } else {
                // pass it through
                midiOut.addEvent(midiMessage, sampleNumber);
            }
        } else if (midiMessage.isNoteOn()) {
            // what is this note relative to the key?
            int noteInKey = ((midiMessage.getNoteNumber() + 12) - paramKey->getIndex()) % 12;
            // what notes would be in this chord relative to the key?
            std::vector<int> chordNotes;
            switch(noteInKey) {
                case 0:
                    chordNotes.push_back(0); chordNotes.push_back(4); chordNotes.push_back(7); break;
                case 2:
                    chordNotes.push_back(2); chordNotes.push_back(5); chordNotes.push_back(9); break;
                case 4:
                    chordNotes.push_back(4); chordNotes.push_back(7); chordNotes.push_back(11); break;
                case 5:
                    chordNotes.push_back(5); chordNotes.push_back(9); chordNotes.push_back(0); break;
                case 7:
                    chordNotes.push_back(7); chordNotes.push_back(11); chordNotes.push_back(2); break;
                case 9:
                    chordNotes.push_back(9); chordNotes.push_back(0); chordNotes.push_back(4); break;
                case 11:
                    chordNotes.push_back(11); chordNotes.push_back(2); chordNotes.push_back(5); break;
            }
            // if there aren't any notes in the chord, it means that the root note wasn't in the key
            if (chordNotes.size() > 0) {
                int oldSampleNumber = sampleNumber - 1;
                if (oldSampleNumber < 0) oldSampleNumber = 0;
                int newSampleNumber = oldSampleNumber + 1;
                // turn off any old notes that are still sounding
                for (int i = 0; i < 128; i++) {
                    if (noteSounding[i]) {
                        midiOut.addEvent(midiMessage.noteOff(1, i), oldSampleNumber);
                        noteSounding[i] = false;
                    }
                }
                // set the current note
                currentNote = midiMessage.getNoteNumber();
                // now take the chord notes and transpose them into the proper key, keeping them in the 0-11 range
                std::vector<int> allowedNotes;
                for (int i=0; i<chordNotes.size(); i++) {
                    int note = chordNotes.at(i);
                    note = (note + paramKey->getIndex()) % 12;
                    allowedNotes.push_back(note);
                }
                // and sound the new notes
                for (int note=paramLowNote->get(); note<=paramHighNote->get(); note++) {
                    // is this note allowed?
                    int wrappedNote = note % 12;
                    for (int i=0; i<allowedNotes.size(); i++) {
                        if (allowedNotes.at(i) == wrappedNote) {
                            // do it!
                            midiOut.addEvent(midiMessage.noteOn(1, note, midiMessage.getVelocity()), newSampleNumber);
                            noteSounding[note] = true;
                        }
                    }
                }
            }
        } else if (midiMessage.isNoteOff()) {
            // if it's the current note, turn all of our chord notes off
            if (midiMessage.getNoteNumber() == currentNote) {
                for (int i = 0; i < 128; i++) {
                    if (noteSounding[i]) {
                        midiOut.addEvent(midiMessage.noteOff(1, i), sampleNumber);
                        noteSounding[i] = false;
                    }
                }
                currentNote = -1;
            }
        }
    }
    midiMessages.clear();
    midiMessages = midiOut;
}

//==============================================================================

bool BlockChords::hasEditor() const {
    return false;
}

AudioProcessorEditor* BlockChords::createEditor() {
    return 0;
}

//==============================================================================

void BlockChords::getStateInformation(MemoryBlock& destData) {
    // Create an outer XML element..
    XmlElement xml("MIDIAPTITUDE_BASSLINE_SETTINGS");

    // Store the values of all our parameters, using their param ID as the XML attribute
    for (int i = 0; i < getNumParameters(); ++i)
        if (AudioProcessorParameterWithID * p = dynamic_cast<AudioProcessorParameterWithID*>(getParameters().getUnchecked(i)))
            xml.setAttribute(p->paramID, p->getValue());

    // then use this helper function to stuff it into the binary blob and return it..
    copyXmlToBinary(xml, destData);
}

void BlockChords::setStateInformation(const void* data, int sizeInBytes) {
    // This getXmlFromBinary() helper function retrieves our XML from the binary blob..
    ScopedPointer<XmlElement> xmlState(getXmlFromBinary(data, sizeInBytes));

    if (xmlState != nullptr) {
        // make sure that it's actually our type of XML object..
        if (xmlState->hasTagName("MIDIAPTITUDE_BASSLINE_SETTINGS")) {
            // Now reload our parameters..
            for (int i = 0; i < getNumParameters(); ++i)
                if (AudioProcessorParameterWithID * p = dynamic_cast<AudioProcessorParameterWithID*>(getParameters().getUnchecked(i)))
                    p->setValueNotifyingHost((float)xmlState->getDoubleAttribute(p->paramID, p->getValue()));
        }
    }
}

//==============================================================================
// This creates new instances of the plugin..

AudioProcessor* JUCE_CALLTYPE createPluginFilter() {
    return new BlockChords();
}

#endif