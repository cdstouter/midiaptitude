/* 
 * File:   JuceHeaders.h
 * Author: Caleb
 *
 * Created on March 10, 2016, 9:25 AM
 */

#ifndef JUCEHEADERS_H
#define JUCEHEADERS_H

#include "AppConfig.h"
#include "modules/juce_audio_basics/juce_audio_basics.h"
#include "modules/juce_audio_devices/juce_audio_devices.h"
#include "modules/juce_audio_formats/juce_audio_formats.h"
#include "modules/juce_audio_plugin_client/juce_audio_plugin_client.h"
#include "modules/juce_audio_processors/juce_audio_processors.h"
#include "modules/juce_core/juce_core.h"
#include "modules/juce_cryptography/juce_cryptography.h"
#include "modules/juce_data_structures/juce_data_structures.h"
#include "modules/juce_events/juce_events.h"
//#include "modules/juce_graphics/juce_graphics.h"
//#include "modules/juce_gui_basics/juce_gui_basics.h"
//#include "modules/juce_gui_extra/juce_gui_extra.h"
//#include "modules/juce_opengl/juce_opengl.h"
//#include "modules/juce_video/juce_video.h"

#if ! DONT_SET_USING_JUCE_NAMESPACE
 // If your code uses a lot of JUCE classes, then this will obviously save you
 // a lot of typing, but can be disabled by setting DONT_SET_USING_JUCE_NAMESPACE.
 using namespace juce;
#endif

#if ! JUCE_DONT_DECLARE_PROJECTINFO
namespace ProjectInfo
{
    const char* const projectName = JucePlugin_Name;
    const char* const versionString = JucePlugin_VersionString;
    const int versionNumber = JucePlugin_VersionCode;
}
#endif

#endif /* JUCEHEADERS_H */

