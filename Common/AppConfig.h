/* 
 * File:   AppConfig.h
 * Author: Caleb
 *
 * Created on March 10, 2016, 9:27 AM
 */

#ifndef APPCONFIG_H
#define APPCONFIG_H

// configuration specific to each MidiAptitude plugin
#ifdef MODULE_NODOUBLE
#define JucePlugin_Name                   "MidiAptitude NoDouble"
#define JucePlugin_PluginCode             'MAND'
#define JucePlugin_Version                1.0.0
#define JucePlugin_VersionCode            0x10000 // for some reason it looks like this is in the format v0v0v
#endif
#ifdef MODULE_BASSLINE
#define JucePlugin_Name                   "MidiAptitude Bassline"
#define JucePlugin_PluginCode             'MABL'
#define JucePlugin_Version                1.0.0
#define JucePlugin_VersionCode            0x10000 // for some reason it looks like this is in the format v0v0v
#endif
#ifdef MODULE_BLOCKCHORDS
#define JucePlugin_Name                   "MidiAptitude Block Chords"
#define JucePlugin_PluginCode             'MABC'
#define JucePlugin_Version                1.0.0
#define JucePlugin_VersionCode            0x10000 // for some reason it looks like this is in the format v0v0v
#endif
#ifdef MODULE_MAPCC
#define JucePlugin_Name                   "MidiAptitude Map CC"
#define JucePlugin_PluginCode             'MAMC'
#define JucePlugin_Version                1.0.0
#define JucePlugin_VersionCode            0x10000 // for some reason it looks like this is in the format v0v0v
#endif
#ifdef MODULE_ALLOWCC
#define JucePlugin_Name                   "MidiAptitude Allow CC"
#define JucePlugin_PluginCode             'MAAC'
#define JucePlugin_Version                1.0.0
#define JucePlugin_VersionCode            0x10000 // for some reason it looks like this is in the format v0v0v
#endif

// configuration that is common to all of the MidiAptitude plugins
#define JUCE_PLUGINHOST_VST 1
#define JucePlugin_Build_VST 1
#define JucePlugin_Manufacturer           "Caleb Stouter"
#define JucePlugin_ManufacturerCode       'CSMA'
#define JucePlugin_Desc                   JucePlugin_Name
#define JucePlugin_IsSynth                1
#define JucePlugin_WantsMidiInput         1
#define JucePlugin_ProducesMidiOutput     1
#define JucePlugin_IsMidiEffect           1
#define JucePlugin_VersionString          "JucePlugin_Version"
#define JucePlugin_VSTCategory            kPlugCategSynth

#define JUCE_MODULE_AVAILABLE_juce_audio_basics             1
#define JUCE_MODULE_AVAILABLE_juce_audio_devices            0
#define JUCE_MODULE_AVAILABLE_juce_audio_formats            0
#define JUCE_MODULE_AVAILABLE_juce_audio_plugin_client      1
#define JUCE_MODULE_AVAILABLE_juce_audio_processors         1
#define JUCE_MODULE_AVAILABLE_juce_core                     1
#define JUCE_MODULE_AVAILABLE_juce_cryptography             0
#define JUCE_MODULE_AVAILABLE_juce_data_structures          1
#define JUCE_MODULE_AVAILABLE_juce_events                   1
#define JUCE_MODULE_AVAILABLE_juce_graphics                 1
#define JUCE_MODULE_AVAILABLE_juce_gui_basics               1
#define JUCE_MODULE_AVAILABLE_juce_gui_extra                0
#define JUCE_MODULE_AVAILABLE_juce_opengl                   0
#define JUCE_MODULE_AVAILABLE_juce_video                    0

//==============================================================================
#ifndef    JUCE_STANDALONE_APPLICATION
 #define   JUCE_STANDALONE_APPLICATION 0
#endif

//==============================================================================
// juce_audio_devices flags:

#ifndef    JUCE_ASIO
 #define   JUCE_ASIO 0
#endif

#ifndef    JUCE_WASAPI
 #define   JUCE_WASAPI 0
#endif

#ifndef    JUCE_WASAPI_EXCLUSIVE
 #define   JUCE_WASAPI_EXCLUSIVE 0
#endif

#ifndef    JUCE_DIRECTSOUND
 #define   JUCE_DIRECTSOUND 0
#endif

#ifndef    JUCE_ALSA
 #define   JUCE_ALSA 0
#endif

#ifndef    JUCE_JACK
 #define   JUCE_JACK 0
#endif

#ifndef    JUCE_USE_ANDROID_OPENSLES
 #define   JUCE_USE_ANDROID_OPENSLES 0
#endif

#ifndef    JUCE_USE_CDREADER
 #define   JUCE_USE_CDREADER 0
#endif

#ifndef    JUCE_USE_CDBURNER
 #define   JUCE_USE_CDBURNER 0
#endif

//==============================================================================
// juce_audio_formats flags:

#ifndef    JUCE_USE_FLAC
 #define   JUCE_USE_FLAC 0
#endif

#ifndef    JUCE_USE_OGGVORBIS
 #define   JUCE_USE_OGGVORBIS 0
#endif

#ifndef    JUCE_USE_MP3AUDIOFORMAT
 #define   JUCE_USE_MP3AUDIOFORMAT 0
#endif

#ifndef    JUCE_USE_LAME_AUDIO_FORMAT
 #define   JUCE_USE_LAME_AUDIO_FORMAT 0
#endif

#ifndef    JUCE_USE_WINDOWS_MEDIA_FORMAT
 #define   JUCE_USE_WINDOWS_MEDIA_FORMAT 0
#endif

//==============================================================================
// juce_audio_processors flags:

#ifndef    JUCE_PLUGINHOST_VST
 #define   JUCE_PLUGINHOST_VST 0
#endif

#ifndef    JUCE_PLUGINHOST_VST3
 #define   JUCE_PLUGINHOST_VST3 0
#endif

#ifndef    JUCE_PLUGINHOST_AU
 #define   JUCE_PLUGINHOST_AU 0
#endif

//==============================================================================
// juce_core flags:

#ifndef    JUCE_FORCE_DEBUG
 //#define JUCE_FORCE_DEBUG
#endif

#ifndef    JUCE_LOG_ASSERTIONS
 //#define JUCE_LOG_ASSERTIONS
#endif

#ifndef    JUCE_CHECK_MEMORY_LEAKS
 //#define JUCE_CHECK_MEMORY_LEAKS
#endif

#ifndef    JUCE_DONT_AUTOLINK_TO_WIN32_LIBRARIES
 //#define JUCE_DONT_AUTOLINK_TO_WIN32_LIBRARIES
#endif

#ifndef    JUCE_INCLUDE_ZLIB_CODE
 //#define JUCE_INCLUDE_ZLIB_CODE
#endif

#ifndef    JUCE_USE_CURL
 //#define JUCE_USE_CURL
#endif

//==============================================================================
// juce_graphics flags:

#ifndef    JUCE_USE_COREIMAGE_LOADER
 #define   JUCE_USE_COREIMAGE_LOADER 0
#endif

#ifndef    JUCE_USE_DIRECTWRITE
 #define   JUCE_USE_DIRECTWRITE 0
#endif

//==============================================================================
// juce_gui_basics flags:

#ifndef    JUCE_ENABLE_REPAINT_DEBUGGING
 //#define JUCE_ENABLE_REPAINT_DEBUGGING
#endif

#ifndef    JUCE_USE_XSHM
 //#define JUCE_USE_XSHM
#endif

#ifndef    JUCE_USE_XRENDER
 //#define JUCE_USE_XRENDER
#endif

#ifndef    JUCE_USE_XCURSOR
 //#define JUCE_USE_XCURSOR
#endif

//==============================================================================
// juce_gui_extra flags:

#ifndef    JUCE_WEB_BROWSER
 #define   JUCE_WEB_BROWSER 0
#endif

#ifndef    JUCE_ENABLE_LIVE_CONSTANT_EDITOR
 #define   JUCE_ENABLE_LIVE_CONSTANT_EDITOR 0
#endif

//==============================================================================
// juce_video flags:

#ifndef    JUCE_DIRECTSHOW
 #define   JUCE_DIRECTSHOW 0
#endif

#ifndef    JUCE_MEDIAFOUNDATION
 #define   JUCE_MEDIAFOUNDATION 0
#endif

#ifndef    JUCE_QUICKTIME
 #define   JUCE_QUICKTIME 0
#endif

#ifndef    JUCE_USE_CAMERA
 #define   JUCE_USE_CAMERA 0
#endif


//==============================================================================
// Audio plugin settings..

#ifndef  JucePlugin_Build_VST
 #define JucePlugin_Build_VST              0
#endif
#ifndef  JucePlugin_Build_VST3
 #define JucePlugin_Build_VST3             0
#endif
#ifndef  JucePlugin_Build_AU
 #define JucePlugin_Build_AU               0
#endif
#ifndef  JucePlugin_Build_RTAS
 #define JucePlugin_Build_RTAS             0
#endif
#ifndef  JucePlugin_Build_AAX
 #define JucePlugin_Build_AAX              0
#endif
#ifndef  JucePlugin_Name
 #define JucePlugin_Name                   ""
#endif
#ifndef  JucePlugin_Desc
 #define JucePlugin_Desc                   ""
#endif
#ifndef  JucePlugin_Manufacturer
 #define JucePlugin_Manufacturer           ""
#endif
#ifndef  JucePlugin_ManufacturerWebsite
 #define JucePlugin_ManufacturerWebsite    ""
#endif
#ifndef  JucePlugin_ManufacturerEmail
 #define JucePlugin_ManufacturerEmail      ""
#endif
#ifndef  JucePlugin_ManufacturerCode
 #define JucePlugin_ManufacturerCode       ''
#endif
#ifndef  JucePlugin_PluginCode
 #define JucePlugin_PluginCode             ''
#endif
#ifndef  JucePlugin_IsSynth
 #define JucePlugin_IsSynth                0
#endif
#ifndef  JucePlugin_WantsMidiInput
 #define JucePlugin_WantsMidiInput         0
#endif
#ifndef  JucePlugin_ProducesMidiOutput
 #define JucePlugin_ProducesMidiOutput     0
#endif
#ifndef  JucePlugin_IsMidiEffect
 #define JucePlugin_IsMidiEffect           0
#endif
#ifndef  JucePlugin_SilenceInProducesSilenceOut
 #define JucePlugin_SilenceInProducesSilenceOut  0
#endif
#ifndef  JucePlugin_EditorRequiresKeyboardFocus
 #define JucePlugin_EditorRequiresKeyboardFocus  0
#endif
#ifndef  JucePlugin_Version
 #define JucePlugin_Version                1.0.0
#endif
#ifndef  JucePlugin_VersionCode
 #define JucePlugin_VersionCode            0x10000
#endif
#ifndef  JucePlugin_VersionString
 #define JucePlugin_VersionString          "1.0.0"
#endif
#ifndef  JucePlugin_VSTUniqueID
 #define JucePlugin_VSTUniqueID            JucePlugin_PluginCode
#endif
#ifndef  JucePlugin_VSTCategory
 #define JucePlugin_VSTCategory            kPlugCategSynth
#endif
#ifndef  JucePlugin_AUMainType
 #define JucePlugin_AUMainType             kAudioUnitType_MusicDevice
#endif
#ifndef  JucePlugin_AUSubType
 #define JucePlugin_AUSubType              JucePlugin_PluginCode
#endif
#ifndef  JucePlugin_AUExportPrefix
 #define JucePlugin_AUExportPrefix         PluginAU
#endif
#ifndef  JucePlugin_AUExportPrefixQuoted
 #define JucePlugin_AUExportPrefixQuoted   "PluginAU"
#endif
#ifndef  JucePlugin_AUManufacturerCode
 #define JucePlugin_AUManufacturerCode     JucePlugin_ManufacturerCode
#endif
#ifndef  JucePlugin_CFBundleIdentifier
 #define JucePlugin_CFBundleIdentifier     com.org.Plugin
#endif
#ifndef  JucePlugin_RTASCategory
 #define JucePlugin_RTASCategory           ePlugInCategory_SWGenerators
#endif
#ifndef  JucePlugin_RTASManufacturerCode
 #define JucePlugin_RTASManufacturerCode   JucePlugin_ManufacturerCode
#endif
#ifndef  JucePlugin_RTASProductId
 #define JucePlugin_RTASProductId          JucePlugin_PluginCode
#endif
#ifndef  JucePlugin_RTASDisableBypass
 #define JucePlugin_RTASDisableBypass      0
#endif
#ifndef  JucePlugin_RTASDisableMultiMono
 #define JucePlugin_RTASDisableMultiMono   0
#endif
#ifndef  JucePlugin_AAXIdentifier
 #define JucePlugin_AAXIdentifier          com.org.Plugin
#endif
#ifndef  JucePlugin_AAXManufacturerCode
 #define JucePlugin_AAXManufacturerCode    JucePlugin_ManufacturerCode
#endif
#ifndef  JucePlugin_AAXProductId
 #define JucePlugin_AAXProductId           JucePlugin_PluginCode
#endif
#ifndef  JucePlugin_AAXCategory
 #define JucePlugin_AAXCategory            AAX_ePlugInCategory_Dynamics
#endif
#ifndef  JucePlugin_AAXDisableBypass
 #define JucePlugin_AAXDisableBypass       0
#endif
#ifndef  JucePlugin_AAXDisableMultiMono
 #define JucePlugin_AAXDisableMultiMono    0
#endif

#endif /* APPCONFIG_H */

