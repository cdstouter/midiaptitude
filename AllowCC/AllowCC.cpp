/* 
 * File:   AllowCC.cpp
 * Author: Caleb
 * 
 * Created on September 17, 2016, 4:39 PM
 */

#ifdef MODULE_ALLOWCC

#include "AllowCC.h"

#include <sstream>

//==============================================================================

AllowCC::AllowCC() {
	for (int i=0; i<numAllows; i++) {
		std::ostringstream stream;
		stream << "allow" << i;
		paramAllow[i] = new AudioParameterInt(stream.str(), "CC to allow", 0, 127, 0);
		addParameter(paramAllow[i]);
	}
}

AllowCC::~AllowCC() { }

//==============================================================================

const String AllowCC::getName() const {
    return JucePlugin_Name;
}

bool AllowCC::acceptsMidi() const {
#if JucePlugin_WantsMidiInput
    return true;
#else
    return false;
#endif
}

bool AllowCC::producesMidi() const {
#if JucePlugin_ProducesMidiOutput
    return true;
#else
    return false;
#endif
}

bool AllowCC::silenceInProducesSilenceOut() const {
    return false;
}

double AllowCC::getTailLengthSeconds() const {
    return 0.0;
}

int AllowCC::getNumPrograms() {
    return 1; // NB: some hosts don't cope very well if you tell them there are 0 programs,
    // so this should be at least 1, even if you're not really implementing programs.
}

int AllowCC::getCurrentProgram() {
    return 0;
}

void AllowCC::setCurrentProgram(int index) { }

const String AllowCC::getProgramName(int index) {
    return String();
}

void AllowCC::changeProgramName(int index, const String& newName) { }

//==============================================================================

void AllowCC::prepareToPlay(double sampleRate, int samplesPerBlock) {
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
}

void AllowCC::releaseResources() {
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

void AllowCC::processBlock(AudioSampleBuffer& buffer, MidiBuffer& midiMessages) {
    // process the midi!
    MidiBuffer::Iterator midiMessagesIterator(midiMessages);
    MidiMessage midiMessage(0);
    MidiBuffer midiOut;
    int sampleNumber;
    while (midiMessagesIterator.getNextEvent(midiMessage, sampleNumber)) {
		if (midiMessage.isController()) {
			// it is one of the allowed CCs?
			bool allowed = false;
			int cc = midiMessage.getControllerNumber();
			for (int i=0; i<numAllows; i++) {
				if (cc == paramAllow[i]->get()) {
					allowed = true;
					break;
				}
			}
			if (allowed) {
				// pass it through unmodified
				midiOut.addEvent(midiMessage, sampleNumber);
			}
		} else {
			// pass it through unmodified
			midiOut.addEvent(midiMessage, sampleNumber);
		}
    }
    midiMessages.clear();
    midiMessages = midiOut;
}

//==============================================================================

bool AllowCC::hasEditor() const {
    return false;
}

AudioProcessorEditor* AllowCC::createEditor() {
    return 0;
}

//==============================================================================

void AllowCC::getStateInformation(MemoryBlock& destData) {
    // Create an outer XML element..
    XmlElement xml("MIDIAPTITUDE_ALLOWCC_SETTINGS");

    // Store the values of all our parameters, using their param ID as the XML attribute
    for (int i = 0; i < getNumParameters(); ++i)
        if (AudioProcessorParameterWithID * p = dynamic_cast<AudioProcessorParameterWithID*>(getParameters().getUnchecked(i)))
            xml.setAttribute(p->paramID, p->getValue());

    // then use this helper function to stuff it into the binary blob and return it..
    copyXmlToBinary(xml, destData);
}

void AllowCC::setStateInformation(const void* data, int sizeInBytes) {
    // This getXmlFromBinary() helper function retrieves our XML from the binary blob..
    ScopedPointer<XmlElement> xmlState(getXmlFromBinary(data, sizeInBytes));

    if (xmlState != nullptr) {
        // make sure that it's actually our type of XML object..
        if (xmlState->hasTagName("MIDIAPTITUDE_ALLOWCC_SETTINGS")) {
            // Now reload our parameters..
            for (int i = 0; i < getNumParameters(); ++i)
                if (AudioProcessorParameterWithID * p = dynamic_cast<AudioProcessorParameterWithID*>(getParameters().getUnchecked(i)))
                    p->setValueNotifyingHost((float)xmlState->getDoubleAttribute(p->paramID, p->getValue()));
        }
    }
}

//==============================================================================
// This creates new instances of the plugin..

AudioProcessor* JUCE_CALLTYPE createPluginFilter() {
    return new AllowCC();
}

#endif
