/*
 * This class was taken directly from a StackOverflow question.
 * The answer was submitted by Christopher Oicles, with no mention of a license:
 * http://stackoverflow.com/a/16538813
 */

/* 
 * File:   CConsole.h
 * Author: Caleb
 *
 * Created on March 10, 2016, 8:27 AM
 */

#ifndef CCONSOLE_H
#define CCONSOLE_H

#include <stdio.h>

class CConsole {
    FILE m_OldStdin, m_OldStdout;
    bool m_OwnConsole;
public:
    CConsole();
    ~CConsole();
};

#endif /* CCONSOLE_H */

