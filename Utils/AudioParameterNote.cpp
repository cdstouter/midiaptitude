/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AudioParameterNote.cpp
 * Author: Caleb
 * 
 * Created on March 16, 2016, 10:32 AM
 */

#include "AudioParameterNote.h"

#include <string>
#include <sstream>

AudioParameterNote::AudioParameterNote(String pid, String nm, int mn, int mx, int def)
: AudioProcessorParameterWithID(pid, nm),
minValue(mn), maxValue(mx),
value((float)def),
defaultValue(convertTo0to1(def)) {
    jassert(minValue < maxValue); // must have a non-zero range of values!
}

AudioParameterNote::~AudioParameterNote() { }

int AudioParameterNote::limitRange(int v) const noexcept {
    return jlimit(minValue, maxValue, v);
}

float AudioParameterNote::convertTo0to1(int v) const noexcept {
    return (limitRange(v) - minValue) / (float)(maxValue - minValue);
}

int AudioParameterNote::convertFrom0to1(float v) const noexcept {
    return limitRange(roundToInt((v * (float)(maxValue - minValue)) + minValue));
}

float AudioParameterNote::getValue() const {
    return convertTo0to1(roundToInt(value));
}

void AudioParameterNote::setValue(float newValue) {
    value = (float)convertFrom0to1(newValue);
}

float AudioParameterNote::getDefaultValue() const {
    return defaultValue;
}

int AudioParameterNote::getNumSteps() const {
    return AudioProcessorParameterWithID::getNumSteps();
}

float AudioParameterNote::getValueForText(const String& text) const {
    return convertTo0to1(text.getIntValue());
}

String AudioParameterNote::getText(float v, int /*length*/) const {
    int midiNote = convertFrom0to1(v);
    int octave = midiNote / 12;
    int scaleNote = midiNote % 12;
    std::string noteNames[] = {
        "C",
        "C#",
        "D",
        "D#",
        "E",
        "F",
        "F#",
        "G",
        "G#",
        "A",
        "A#",
        "B"
    };
    std::ostringstream text;
    text << midiNote << " " << noteNames[scaleNote] << octave;
    return String(text.str());
    //return String(convertFrom0to1(v));
}

AudioParameterNote& AudioParameterNote::operator=(int newValue) {
    const float normalisedValue = convertTo0to1(newValue);

    if (value != normalisedValue)
        setValueNotifyingHost(normalisedValue);

    return *this;
}
