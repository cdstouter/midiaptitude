/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AudioParameterNote.h
 * Author: Caleb
 *
 * Created on March 16, 2016, 10:32 AM
 */

#ifndef AUDIOPARAMETERNOTE_H
#define AUDIOPARAMETERNOTE_H

/*
 * A variation on AudioParameterInt that displays midi note names as well
 */
class JUCE_API AudioParameterNote : public AudioProcessorParameterWithID {
public:
    /** Creates an AudioParameterNote with an ID, name, and range.
        Note that the min and max range values are inclusive.
        On creation, its value is set to the default value.
     */
    AudioParameterNote(String parameterID, String name,
                       int minValue, int maxValue,
                       int defaultValue);

    /** Destructor. */
    ~AudioParameterNote();

    /** Returns the parameter's current value as an integer. */
    int get() const noexcept {
        return roundToInt(value);
    }

    /** Returns the parameter's current value as an integer. */
    operator int() const noexcept {
        return get();
    }

    /** Changes the parameter's current value to a new integer.
        The value passed in will be snapped to the permitted range before being used.
     */
    AudioParameterNote& operator=(int newValue);

    /** Returns the parameter's range. */
    Range<int> getRange() const noexcept {
        return Range<int> (minValue, maxValue);
    }


private:
    //==============================================================================
    int minValue, maxValue;
    float value, defaultValue;

    float getValue() const override;
    void setValue(float newValue) override;
    float getDefaultValue() const override;
    int getNumSteps() const override;
    String getText(float, int) const override;
    float getValueForText(const String&) const override;

    int limitRange(int) const noexcept;
    float convertTo0to1(int) const noexcept;
    int convertFrom0to1(float) const noexcept;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(AudioParameterNote)
};

#endif /* AUDIOPARAMETERNOTE_H */

